##Utilizando um repositorio remoto gitlab - Parte 1
###Comandos:

`git clone https://gitlab.com/gabrielsantosvi97/pratica1.git`

O comando foi utilizado para clonar o repositorio remoto, o git clone é usado sobretudo para apontar para um repositório existente e fazer um clone ou cópia deste repositório no novo diretório, em outro local. O repositório original pode estar localizado no sistema de arquivos local ou em protocolos com suporte a acesso por máquinas remotas.

`git add README.md`

O comando foi utilizado para adicionar o arquivo readme.md para o diretorio local, esse comando realiza a inclusão ou modificação do arquivo no diretório local, preparando ele para ser entregue ao servidor remoto para a mesma aplicação que está sincronizada na máquina local.

