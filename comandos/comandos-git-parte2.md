##Utilizando um repositorio remoto gitlab - Parte 2
###Comandos:

`git commit -m "descrição da alteração"`

O comando foi utilizado para salvar as alterações feitos no repositorio de fato, criando assim um estado do meu código.
O Git commit permite que você crie um commit, ou seja, você consegue guardar o estado do seu repositório naquele momento. Existem diferentes estratégias para fazer commits, mas a ideia principal é que a cada ponto em que o seu código esteja funcionando com uma nova pequena funcionalidade, exista um commit. 

`git push -u origin main `

O git push foi utilizado para enviar as alterações para o repositorio remoto. O comando git push é usado para enviar o conteúdo do repositório local para um repositório remoto. O comando push transfere commits do repositório local a um repositório remoto.


`git pull`

O git pull foi utilizado para atualizar as alterações para o repositorio remoto. 